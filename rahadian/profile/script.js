// Variable declaration


function prompt_awal () {
    let angka1;
    let angka2;
    let angka3;
    let oper;
    let hasil;
    angka1 = prompt('Masukkan angka1');
    if (angka1 === null) return;

    if (isNaN(angka1)) {
        do {
            angka1 = prompt('Harus angka!\nMasukkan angka1');
            if (angka1 === null) return;
        } while (isNaN(angka1));
    }

    angka2 = prompt('Masukkan angka2');
    if (angka2 === null) return;

    if (isNaN(angka2)) {
        do {
            angka2 = prompt('Harus angka!\nMasukkan angka2');
            if (angka2 === null) return;
        } while (isNaN(angka2));
    }

    angka3 = prompt('Masukkan angka3');
    if (angka3 === null) return;

    if (isNaN(angka3)) {
        do {
            angka3 = prompt('Harus angka!\nMasukkan angka3');
            if (angka3 === null) return;
        } while (isNaN(angka3));
    }

    // convert to number
    angka1 = Number(angka1);
    angka2 = Number(angka2);
    angka3 = Number(angka3);

    oper = prompt(`Angka 1: ${angka1} \nAngka 2: ${angka2} \nAngka 3: ${angka3} \nMasukkan operator: kali/bagi/kurang/tambah`);
    if (oper === null) return;

    function math_op(operator) {
        let a;
        switch (operator){
            case 'tambah':
                a = angka1 + angka2 + angka3;
                return a;
            case 'kurang':
                a = angka1 - angka2 - angka3;
                return a;
            case 'kali':
                a = angka1 * angka2 * angka3;
                return a;
            case 'bagi':
                a = angka1 / angka2 / angka3;
                return a;
            case null:
                a = null;
                return a;
            default:
                while (true) {
                    operator = prompt('Operator salah. Masukkan operator: kali/bagi/kurang/tambah');
                    if (operator === null) return;
                    return math_op(operator);
                }
            }
        }

    hasil = math_op(oper);
    let elemen_judul;
    let elemen_isil;
    if (hasil != null) {
        alert(hasil);
        elemen_judul = document.querySelector('#tempat-angka');
        elemen_judul.innerHTML="Angkamu tadi: ";
        elemen_isi = document.querySelector('#angka');
        elemen_isi.innerHTML=hasil;
        console.log(hasil);
    } else{
        return;
    }

}

prompt_awal();
let isAgain = confirm('Coba lagi?');
if (isAgain) location.reload();